/**
 * Created by sirius on 5/12/17.
 */
let router      = require('express').Router(),
    userCtrl    = require('../../controllers/users/user.controller.js'),
    isAdmin     = require('../../setup/auth.js').isAdmin,
    isUser      = require('../../setup/auth.js').isUser,
    passport    = require('../../setup/auth.js').passport,
    validate    = require('../../validation/user.validator.js');

router
    .post('/signup',                                                                    validate('userSignUp'), userCtrl.signup)
    .post('/login',                                                                     validate('userLogin'),  userCtrl.login)
    .post('/upload',                                                                    /*upload.single('image'),*/ userCtrl.upload)
//================================= User routes  =================================================================================
    .get('/user',           passport.authenticate('jwt',{ session: false}), isUser,                            userCtrl.userGet)
    .put('/user',           passport.authenticate('jwt',{ session: false}), isUser,    validate('userEdit'),   userCtrl.userEdit)
//================================= Admin routes =================================================================================
    .post('/user',          passport.authenticate('jwt',{ session: false}), isAdmin,   validate('userCreate'), userCtrl.userCreate)
    .get('/users',          passport.authenticate('jwt',{ session: false}), isAdmin,                           userCtrl.userGetAll)
    .get('/user/:id',       passport.authenticate('jwt',{ session: false}), isAdmin,                           userCtrl.userGet)
    .put('/user/:id',       passport.authenticate('jwt',{ session: false}), isAdmin,   validate('userEdit'),   userCtrl.userEdit)
    .delete('/user/:id',    passport.authenticate('jwt',{ session: false}), isAdmin,                           userCtrl.userDelete);

module.exports = router;
