/**
 * Created by sirius on 5/12/17.
 */

const db = require("./api/setup/db.js");
const app = require('./api/setup/express.js');
const chalk = require('chalk');

const preferedPort = 8088,
    port = process.env.PORT || preferedPort;
// listen (start app with node server.js) ==================================
app.listen(port);
console.log(chalk.red('BeeWeb api\t\t\t started'));
console.log(chalk.blue('Port:\t\t\t\t '+port));
console.log(chalk.yellow('Database:\t\t\t BeeWeb'));